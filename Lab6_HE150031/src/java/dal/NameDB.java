/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Name;

/**
 *
 * @author Admin
 */
public class NameDB extends DBContext {

    public ArrayList<Name> getNameWithPage(int page, int page_size) {
        ArrayList<Name> list = new ArrayList<>();
        try {
            String sql = "select * FROM (SELECT ROW_NUMBER() OVER (ORDER BY ID asc) as MyRowNumber,* FROM DummyTBL) d\n"
                    + "Where d.MyRowNumber BETWEEN(((?-1)*?)+1) AND (?*?)";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, page);
            stm.setInt(2, page_size);
            stm.setInt(3, page);
            stm.setInt(4, page_size);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Name s = new Name();
                s.setID(rs.getInt("ID"));
                s.setName(rs.getString("Name"));
                list.add(s);
            }
        } catch (SQLException ex) {
            Logger.getLogger(NameDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public int countStudents() {
        try {
            String sql = "SELECT COUNT(*) FROM DummyTBL";
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(NameDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
      
}
}
