<%-- 
    Document   : list.jsp
    Created on : Mar 10, 2022, 9:38:17 AM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>

        <table border="1px solid black">
            <tr>
                <th style="width:150px">ID</th>
                <th style="width:300px">Name</th>
            </tr>

            <c:forEach items="${name}" var="s">
                <tr <c:if  test="${s.ID % 2 eq 1}"> style="background-color: gray" </c:if>>
                    <td>${s.ID}</td>
                    <td>${s.getName()}</td>
                </tr>
            </c:forEach>

        </table>
        <a href="List?page=1">First</a>

        <c:if test="${page == 1}">
            <b>${page}</b>
            <a href="List?page=${page+1}">${page+1}</a>
            <a href="List?page=${page+2}">${page+2}</a>
            <a href="List?page=${page+3}">${page+3}</a>
            <a href="List?page=${page+4}">${page+4}</a>
        </c:if>
        <c:if test="${page == 2 }">
            <a href="List?page=${page-1}">${page-1}</a>
            <b>${page}</b>
            <a href="List?page=${page+1}">${page+1}</a>
            <a href="List?page=${page+2}">${page+2}</a>
            <a href="List?page=${page+3}">${page+3}</a>
        </c:if>
        <c:if test="${page >=3 && page <(totalPage-2)}">
            <a href="List?page=${page-2}">${page-2}</a>
            <a href="List?page=${page-1}">${page-1}</a>
            <b>${page}</b>
            <a href="List?page=${page+1}">${page+1}</a>
            <a href="List?page=${page+2}">${page+2}</a>
        </c:if>
        <c:if test="${page == totalPage}">
            <a href="List?page=${page-4}">${page-4}</a>
            <a href="List?page=${page-3}">${page-3}</a>
            <a href="List?page=${page-2}">${page-2}</a>
            <a href="List?page=${page-1}">${page-1}</a>
            <b>${page}</b>
        </c:if>
        <c:if test="${page == (totalPage-2)}">
            <a href="List?page=${page-3}">${page-3}</a>
            <a href="List?page=${page-2}">${page-2}</a>
            <a href="List?page=${page-1}">${page-1}</a>
            <b>${page}</b>
            <a href="List?page=${page+1}">${page+1}</a>
        </c:if>
        <a href="List?page=${totalPage}">Last</a>
    </body>
</html>
