/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal.inf;

import java.util.ArrayList;
import model.User;

/**
 *
 * @author Admin
 */
public interface UserDBinf {
    
    public User getUserLogin(String email, String password) throws Exception;
    
    public User getUserById(int userId) throws Exception;
    
    public ArrayList<User> getAllUser() throws Exception;
    
    public boolean checkEmail() throws Exception;
    
    public void updateUser(User a) throws Exception;
    
    public void updatePass(int userID, String pass) throws Exception;
    
}
